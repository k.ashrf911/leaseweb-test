<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class RamModuleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'size' => 8,
            'type' => 'ddr5'
        ];
    }
}
