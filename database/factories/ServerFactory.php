<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ServerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'assetId' => '1156687',
            'brand' => 'HP',
            'name' => 'IC889',
            'price' => 15.36
        ];
    }
}
