<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateRamModulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ram_modules', function (Blueprint $table) {
            $table->uuid('id')->unique()->primary();
            $table->string('server_id',255);
            $table->foreign('server_id')->references('id')->on('Server')->onDelete('cascade');
            $table->enum('type',['ddr3','ddr4','ddr5']);
            $table->string('size',255);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ram_modules');
    }
}
