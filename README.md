## About LeaseWeb Server Management System

The application is build using Laravel 8.7 and MYSQL as a DB engin , And Jquery for frontend


### Premium Partners

- **Start By cloning the project to your local server**
- **Run the following command " _composer install_ "**
- **Make sure you create .env file and adjust it with your database credentials**
- **To start the server run the following command " _php artisan serve_ "**
- **Test Cases are placed under tests/Feature/ServerTest.php**
- **To run the tests use the following command " _php artisan test_ " and you will get the results of the DB**
- **I used Jquery , Boostrap for the frontend , and Jquery Datatables**
- **The backend is built as RESTFUL API , to serve the frontend side of the project and incoming calls from outside the project using http calls**
- **Unit tests are using DB Factories to seed data for testing purposes**
- **ORM is being used for database management for faster and efficient database processes**
- **[POSTMAN Workspace for the API calls](https://documenter.getpostman.com/view/2088611/2s7YYr7PcH)**
