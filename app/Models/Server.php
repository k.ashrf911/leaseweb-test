<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Server extends Model
{
    use HasFactory;
    protected $table = 'server';
    public $incrementing = false;
    public $fillable = ['assetId','brand','name','price'];

    public $casts = [
        'id' => 'string',
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model){
            $model->id = (string) Str::uuid();
        });
    }


    public function ram(){
        return $this->hasMany(RamModule::class,'server_id','id');
    }

}
