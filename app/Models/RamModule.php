<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class RamModule extends Model
{
    use HasFactory;
    protected $table = 'ram_modules';
    public $incrementing = false;
    public $timestamps = false;

    public $fillable = ['server_id','type','size'];

    public $casts = [
        'id' => 'string',
        'server_id' => 'string'
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function ($model){
            $model->id = (string) Str::uuid();
        });
    }

    public function server(){
        return $this->belongsTo(Server::class,'id','server_id');
    }
}
