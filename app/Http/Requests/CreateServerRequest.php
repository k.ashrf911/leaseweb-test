<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateServerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $request = $this->request;

        return [
            'assetId' => [
                'required',
                Rule::unique('server')->where(function ($query) use ($request) {
                    return $query->where('assetId', $request->get('assetId'));
                })
            ],
            'brand' => 'required',
            'name' => 'required',
            'price' => 'required|numeric|min:1',
            'ram' => 'required|array|min:1',
            'ram.*.type' => 'required|in:ddr3,ddr4,ddr5',
            'ram.*.size' => 'required|integer'
        ];
    }
}
