<?php

namespace App\Http\Middleware;

use App\Helpers\JsonResponse;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;

class ResponseApi
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $response = $next($request);
        $error = ($response->status() != ResponseAlias::HTTP_OK && $response->status() != ResponseAlias::HTTP_CREATED );
        return JsonResponse::buildResponse(
            ($error || json_decode($response->getContent()) == null) ? [] : json_decode($response->getContent()),
            $response->getStatusCode(),
            ($error)? (json_decode($response->getContent(),true)) : null,
            !$error
        );
    }
}
