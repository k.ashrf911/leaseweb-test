<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateServerRequest;
use App\Models\Server;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;


class ServerController extends Controller
{

    public function __construct(){
        $this->middleware('Response');
    }


    public function index(Request $request){
        return Server::with('ram')->whereHas('ram')
            ->orderBy('created_at','DESC')->get();
    }

    public function show($id){
        return Server::with('ram')
            ->find($id);
    }

    public function create(CreateServerRequest $request){
        $server = Server::create($request->all());
        $server->ram()->createMany($request->ram);

        return $server;
    }

    public function delete($id){
        return Server::find($id)
            ->delete();
    }
}
