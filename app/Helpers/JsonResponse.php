<?php

namespace App\Helpers;

class JsonResponse {
    public static function buildResponse($data,$status,$error = null , $success = true){
        return response()->json([
            'data' => $data,
            'status' => $status,
            'success' => boolval($success),
            'error' => $error
        ],$status)->setEncodingOptions(JSON_NUMERIC_CHECK);
    }
}
