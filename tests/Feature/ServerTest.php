<?php

namespace Tests\Feature;

use App\Models\RamModule;
use App\Models\Server;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ServerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_all_servers()
    {
        Server::factory(2)->create()->each(function($s) {
            $s->ram()->createMany(RamModule::factory(2)->make()->toArray());
        });


        $response = $this->get('/api/all');
        $response->assertStatus(200);
    }

    public function test_get_server()
    {
        $server = Server::factory(1)->create()->each(function($s) {
            $s->ram()->createMany(RamModule::factory(2)->make()->toArray());
        });

        $response = $this->get('/api/show/'.$server->first()->id);
        $response->assertStatus(200);
    }

    public function test_create_server()
    {
        $body = [
            'assetId'=> '558',
            'brand' => 'HP',
            'name' => 'IM44',
            'price' => 4588.36,
            'ram' => [
                [
                    'type'=> 'ddr5',
                    'size' => '5'
                ]
            ]
        ];
        $response = $this->post('/api/create',$body);
        $response->assertStatus(201);
    }

    public function test_delete_server()
    {
        $server = Server::factory(1)->create()->each(function($s) {
            $s->ram()->createMany(RamModule::factory(2)->make()->toArray());
        });

        $response = $this->delete('/api/delete/'.$server->first()->id);
        $response->assertStatus(200);
    }
}
