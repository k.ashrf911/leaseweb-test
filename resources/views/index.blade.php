<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    <title>LeaseWeb Manage Servers</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.min.css">
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>LeaseWeb Manage Servers</h2>
            </div>
            <div class="pull-right" style="margin-bottom: 10px;">
                <button class="btn btn-success" data-toggle="modal" data-target="#newServer" id="newServerModal"> Create New Server</button>
            </div>
        </div>
    </div>

    <table class="table table-responsive" id="my_table" width="100%">
        <thead>
            <tr>
                <th>ID</th>
                <th>Asset ID</th>
                <th>Brand</th>
                <th>Name</th>
                <th>Price</th>
                <th>Created At</th>
                <th>Ram Modules</th>
                <th>Delete</th>
            </tr>
        </thead>
    </table>

    <!-- Modal -->
    <div class="modal fade" id="ramModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">RAM Modules</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Type</th>
                                <th>Size</th>
                            </tr>
                        </thead>

                        <tbody class="ramBody">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="newServer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">New Server</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form id="submitServer" action="/api/create" method="post">
                        <div class="form-group">
                            <label>Asset ID</label>
                            <input class="form-control" type="text" placeholder="Asset ID" name="assetId" required>
                            <p id="errorAssetId" style="color: red"></p>
                        </div>

                        <div class="form-group">
                            <label>Brand</label>
                            <input class="form-control" type="text" name="brand" placeholder="Ex. Dell , HP" required>
                        </div>

                        <div class="form-group">
                            <label>Name</label>
                            <input class="form-control" type="text" name="name" placeholder="Ex. R210,R730" required>
                        </div>

                        <div class="form-group">
                            <label>Price</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                </div>
                                <input type="number" class="form-control" name="price" aria-label="Price" required min="1">
                                <div class="input-group-append">
                                    <span class="input-group-text">.00</span>
                                </div>
                            </div>
                        </div>

                        <h6>RAM Modules</h6>

                        <div id="ramModulesContainer">
                            <div class="row ramModuleRow">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select class="form-control" required name="type">
                                            <option selected disabled value="">Choose Type</option>
                                            <option value="ddr3">DDR3</option>
                                            <option value="ddr4">DDR4</option>
                                            <option value="ddr5">DDR5</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="input-group mb-3">
                                            <input type="number" class="form-control" name="size" placeholder="Size" required min="1">
                                            <div class="input-group-append">
                                                <span class="input-group-text">GB</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <button class="btn btn-primary" type="button" id="addNewRow">
                                        +
                                    </button>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <button class="btn btn-success" type="submit">Submit</button>
                            <div class="spinner-border text-primary" role="status" style="float: right; display: none;" id="spinner">
                                <span class="sr-only"></span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


</div>
    <script src="https://code.jquery.com/jquery-3.6.1.js" integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI=" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>    <script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.min.js"></script>
    <script>
        $(document).ready(function (){
            var selectedServer = []
            $('#my_table').DataTable({
                "processing":true,
                "serverSide":false,
                "ajax": "{{url('api/all')}}",
                "pageLength" : 10,
                order: [[5, 'desc']],
                "columns": [
                    { "data" : "id" },
                    { "data" : "assetId" },
                    { "data" : "brand" },
                    { "data" : "name" },
                    { "data" : "price" },
                    {
                        "data" : "created_at",
                        "render" : function (data,type,row) {
                            let myDate = new Date(data)
                            return myDate.toISOString().split('T')[0]
                        }
                    },
                    {
                        "data" : "ram" ,
                        "render" : function (data,type,row) {
                            const ram = JSON.stringify(row.ram).replace(/"([^"]+)":/g, '$1:').replaceAll("\"", "'")
                            return '<button class="btn btn-primary" data-toggle="modal" data-ram="'+ ram +'" data-target="#ramModal">View Ram Modules</button>'
                        }
                    },
                    {
                        "data" : null ,
                        "render" : function (data,type,row) {
                            return '<button class="btn btn-danger deleteButton" data-id="'+ row.id +'">Delete</button>'
                        }
                    }
                ]
            })

            $('#ramModal').on('show.bs.modal', function (event) {
                const ram = $(event.relatedTarget).data('ram')
                //sanitize json
                $sanitized_json_ram = JSON.parse(ram.replace(/'/g, '"').replace(/(['"])?([a-z0-9A-Z_]+)(['"])?:/g, '"$2": '))

                let ramRows = ''

                $sanitized_json_ram.forEach((ram) => {
                    ramRows += '<tr><td>'+ ram.id +'</td> <td>'+ ram.type.toUpperCase() +'</td> <td>'+ ram.size +' GB</td></tr>'
                })

                $('.ramBody').html(ramRows)
            })


            $(document).on('click', '.deleteButton', function() {
                const id = $(this).attr("data-id")
                if(confirm("Are you sure you want to delete this server?")){
                    $.ajax({
                        url: "/api/delete/" + id,
                        method: 'DELETE',
                        success: function(result){
                            const table = $('#my_table').DataTable();
                            table.ajax.reload();
                        }}
                    );
                }
                else{
                    return false;
                }
            });

            $(document).on('click', '#addNewRow', function() {
                const id = Math.random().toString(36).slice(2, 7);
                $('#ramModulesContainer').append(`
                    <div class="row ramModuleRow" id="`+ id +`">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <select class="form-control" required name="type">
                                            <option selected disabled value="">Choose Type</option>
                                            <option value="ddr3">DDR3</option>
                                            <option value="ddr4">DDR4</option>
                                            <option value="ddr5">DDR5</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <div class="input-group mb-3">
                                            <input type="number" class="form-control" name="size" placeholder="Size" required min="1">
                                            <div class="input-group-append">
                                                <span class="input-group-text">GB</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <button class="btn btn-primary" type="button" id="addNewRow">
                                        +
                                    </button>
                                </div>

                                <div class="col-md-2">
                                    <button class="btn btn-danger" type="button" id="removeRow" data-rowId="`+id+`">
                                        -
                                    </button>
                                </div>
                           </div>
                `);
            });

            $(document).on('click', '#removeRow', function() {
                const id = $(this).attr("data-rowId")
                $('#'+id).remove();
            });

            $(document).on('click','#newServerModal',function () {
                $('#errorAssetId').text('')
                $('#submitServer').trigger("reset");
            })

            $(document).on('submit', '#newServer', function(e) {
                e.preventDefault()
                $('#spinner').show()
                const form = $('#submitServer').serialize()

                const ramArray = []
                const arr = []
                $(".ramModuleRow").each(function(i,div) {
                    arr[i] = $(":input",this).map(function() {
                        return { [$(this).attr("name")]:$(this).val() }
                    }).get()
                })

                arr.forEach((ramInputs) => {
                    const ram = {}
                    ramInputs.forEach(r => {
                        if(r.type){
                            ram.type = r.type
                        }else if(r.size){
                            ram.size = r.size
                        }
                    })

                    ramArray.push(ram)
                })


                $.ajax({
                        url: "/api/create",
                        method: 'POST',
                        data: form+'&'+$.param({ 'ram': ramArray }),
                        success: function(data){
                            $('#spinner').hide()
                            const table = $('#my_table').DataTable();
                            table.ajax.reload();
                            $('#newServer').modal('hide');
                        },
                        error : function (request){
                            $('#spinner').hide()
                            const error = JSON.parse(request.responseText).error.errors
                            $('#errorAssetId').text(error.assetId)
                        }
                    }
                );

            });


        })
    </script>

</body>
</html>
